function createStore() {
  this.data = [];

  this.add = function (todo) {
    this.data.push(todo);
  };

  this.getAll = function () {
    return this.data;
  };

  this.deleteTodo = function (todoId) {
    const filtered = this.data.filter((todo) => {
      return todo.id !== todoId;
    });
    this.data = filtered;
    return todoId;
  };
}

const store = new createStore();

module.exports = store;
