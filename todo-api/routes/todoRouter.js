const express = require("express");
const todoController = require("../controllers/todoController");

const router = express.Router();

router.get("/", (req, res) => {
  return todoController.getAll(req, res);
});

router.post("/", (req, res) => {
  return todoController.add(req, res);
});

router.delete("/:id", (req, res) => {
  return todoController.deleteTodo(req, res);
});

module.exports = router;
