const store = require("../store");
const uuid = require("uuid");

const getAll = (req, res) => {
  setTimeout(() => {
    return res.send(store.getAll());
  }, 1000);
};

const add = (req, res) => {
  const todo = req.body;
  todo.id = uuid.v4();
  store.add(todo);
  setTimeout(() => {
    return res.send(todo);
  }, 1000);
};

const deleteTodo = (req, res) => {
  const todoId = req.params.id;
  store.deleteTodo(todoId);
  res.send();
};

module.exports = { getAll, add, deleteTodo };
