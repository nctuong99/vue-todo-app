const express = require("express");
const cors = require('cors');
const todoRouter = require('./routes/todoRouter');

const app = express();

app.use(cors());
app.use(express.json());


app.get('/ping', (req, res) => { 
    res.send("ok")
})

app.use('/todos', todoRouter);

app.listen(4000, () => {
    console.log(":: listening on port 4000")
})