import { createRouter, createWebHistory } from "vue-router";
import DefaultView from "../views/DefaultView.vue";
import HomeView from "../views/HomeView.vue";
import TodoView from "../views/TodoView.vue";

const routes = [
  {
    path: "/",
    component: DefaultView,
    children: [
      { path: "/", name: "home", component: HomeView },
      { path: "/todo", name: "todo", component: TodoView },
    ],
  },
];

const router = createRouter({
  routes,
  history: createWebHistory(),
});

export default router;
