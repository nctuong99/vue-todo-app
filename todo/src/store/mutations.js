export default {
  getAllTodos: (state, todos) => {
    state.todos = todos;
    state.loading = false;
  },
  addTodo: (state, todo) => {
    state.todos.push(todo);
    state.loading = false;
  },
  setLoading: (state, loading = false) => {
    state.loading = loading;
  },
  deleteTodo: (state, todoId) => {
    const filtered = state.todos.filter((todo) => {
      return todo.id !== todoId;
    });
    state.todos = filtered;
    state.loading = false;
  },
};
