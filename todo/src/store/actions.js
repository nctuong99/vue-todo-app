import axiosClient from "@/services/axiosClient";

export default {
  getAllTodos: ({ commit }) => {
    commit("setLoading", true);
    axiosClient.get("/todos").then((res) => {
      commit("getAllTodos", res.data);
    });
  },

  addTodo: ({ commit }, todo) => {
    const data = {
      title: todo,
      description: "description",
    };
    commit("setLoading", true);
    axiosClient.post("/todos", data).then((res) => {
      commit("addTodo", res.data);
    });
  },

  deleteTodo: ({ commit }, todoId) => {
    commit("setLoading", true);
    axiosClient.delete(`/todos/${todoId}`).then(() => {
      commit("deleteTodo", todoId);
    });
  },
};
